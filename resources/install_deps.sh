#!/bin/sh

set -e -x

cat <<EOF >/etc/apt/sources.list 
deb http://mirror.yandex.ru/ubuntu xenial main restricted
deb-src http://mirror.yandex.ru/ubuntu xenial main restricted

deb http://mirror.yandex.ru/ubuntu xenial universe
deb-src http://mirror.yandex.ru/ubuntu xenial universe

deb http://mirror.yandex.ru/ubuntu xenial multiverse
deb-src http://mirror.yandex.ru/ubuntu xenial multiverse

deb http://mirror.yandex.ru/ubuntu xenial-backports main restricted universe multiverse
deb-src http://mirror.yandex.ru/ubuntu xenial-backports main restricted universe multiverse

deb http://mirror.yandex.ru/ubuntu xenial-security main restricted universe multiverse
deb http://mirror.yandex.ru/ubuntu xenial-updates main restricted universe multiverse
EOF

cat /etc/apt/sources.list

apt-get update

apt-get install -y \
    curl \
    gdb \
    build-essential \
    cmake \
    clang-format \
    clang-tidy \
    g++ \
    libgtest-dev \
    sudo \
    strace \
    python3 \
    python3-pip \
    git

#    libbenchmark-dev \
#    ninja-build \
#    libboost1.62-dev \
#    libboost-fiber1.62-dev \
#    libboost-system1.62-dev \
#    libboost-context1.62-dev \
#    libboost-coroutine1.62-dev \
#    libsnappy-dev \
#    libshad-gtest \

apt-get clean

cd /usr/src/gtest
cmake CMakeLists.txt
make
# make install
# copy or symlink libgtest.a and libgtest_main.a to your /usr/lib folder
sudo cp *.a /usr/lib

mkdir /tmp/foo
cd /tmp/foo
cat <<EOF >tests.cpp
#include <gtest/gtest.h>

TEST(TestGtest, Dummy) {
    ASSERT_EQ(1, 1);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
EOF

cat <<EOF >CMakeLists.txt
cmake_minimum_required(VERSION 3.5)

# Locate GTest
find_package(GTest REQUIRED)
include_directories(\${GTEST_INCLUDE_DIRS})

# Link runTests with what we want to test and the GTest and pthread library
add_executable(runTests tests.cpp)
target_link_libraries(runTests \${GTEST_LIBRARIES} pthread)
EOF

cmake CMakeLists.txt
make
./runTests
cd /
rm -rf /tmp/foo





